import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HomeGame } from 'src/app/shared/interfaces/home';
import { HomeService } from 'src/app/shared/services/home.service';

@Component({
  selector: 'home-page',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  latestGamesArray!: HomeGame[];

  constructor(
    private homeService: HomeService,
    private router: Router
    ) {}

  ngOnInit() {
    this.homeService.getLatestGames()
  }

  viewGame(game: { id: any; }) {
    this.router.navigate(['/game'], { queryParams: {id: game.id} });
  }

}
